<?php
/* Plugin Name: idefix
3	Plugin URI: http://idefix.gr/
4	Description: Update product price API for Idefix
5	Version: 1.0
6	Author: Panos
7	Author URI: http://idefix.gr/
8	License: GPLv2 or later
9	*/

require_once dirname(__FILE__) . '/lib/DonaldApi.php';

 // ------------------------------------------------------------------
 // Add all your sections, fields and settings during admin_init
 // ------------------------------------------------------------------
 //
 
 function slxif_settings_api_init() {
 	// Add the section to reading settings so we can add our
 	// fields to it
 	add_settings_section(
		'slxif_setting_section',
		'Idefix API settings',
		'slxif_setting_section_callback_function',
		'general'
	);
 	
 	// Add the field with the names and function to use for our new
 	// settings, put it in our new section
 	add_settings_field(
		'slxif_api_key',
		'API key for Idefix',
		'slxif_api_key_setting_callback_function',
		'general',
		'slxif_setting_section'
	);
 	
 	// Register our setting so that $_POST handling is done for us and
 	// our callback function just has to echo the <input>
 	register_setting( 'general', 'slxif_api_key' );
 } // eg_settings_api_init()
 
 add_action( 'admin_init', 'slxif_settings_api_init' );
 
  
 // ------------------------------------------------------------------
 // Settings section callback function
 // ------------------------------------------------------------------
 //
 // This function is needed if we added a new section. This function 
 // will be run at the start of our section
 //
 
 function slxif_setting_section_callback_function() {
 	echo '<p>Idefix API settings</p>';
 }
 
 // ------------------------------------------------------------------
 // Callback function for our example setting
 // ------------------------------------------------------------------
 //
 // creates a checkbox true/false option. Other types are surely possible
 //
 
 function slxif_api_key_setting_callback_function() {
 	echo '<input name="slxif_api_key" id="slxif_api_key" type="text" value="'.get_option( 'slxif_api_key' ).'" class="code" />';
 }
 
 // ------------------------------------------------------------------
 // Register REST api endpoint for Idefix
 
add_action( 'rest_api_init', 'slxif_register_routes' ); 

function slxif_register_routes() {
    register_rest_route( 
        'slx-idefix/v1',
        '/priceupdate/',
        array(
            'methods' => 'POST',
            'callback' => 'slx_idefix_priceupdate_post',
        )
    );
}

function slx_idefix_priceupdate_post() {
    $da = new DonaldApi(get_option( 'slxif_api_key' ));
    $action = $da->processProductPost();
    $done = false;
    if( $action ) {
        if( $action ) {  // update price
            // find product by id: $action->getProductId()
            //$pf = new WC_Product_Factory();
            //$product = $pf->get_product($action->getProductId());
            $product = wc_get_product($action->getProductId());
            if($product) {
                // update product price to $action->getNPrice()
                $product->set_price($action->getNPrice());
                $product->save();
                $done= true;
            }
            else {
                $da->setError("product not found");
            }
        }
    }
    if( $done ) {
        $da->responseOK();
    }
    else {
        $da->responseError();
    }
    die();
}