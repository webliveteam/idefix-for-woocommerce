<?php

/**
 * Description of DonaldDisableAction
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 24, 2016
 */
class DonaldDisableAction {

    private $productId;
    private $enabled;
    
    public function __construct($productId, $enabled) {
       $this->productId = $productId;
       $this->enabled = $enabled;
    }
    
    public function getProductId() {
        return $this->productId;
    }

    public function getEnabled() {
        return $this->enabled;
    }
    public function setProductId($productId) {
        $this->productId = $productId;
        return $this;
    }

    public function setEnabled($enabled) {
        $this->enabled = $enabled;
        return $this;
    }

    public function getAction() {
        return 'disable';
    }
}
