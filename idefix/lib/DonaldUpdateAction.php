<?php

/**
 * Description of DonaldUpdateAction
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 24, 2016
 */
class DonaldUpdateAction {
    private $productId;
    private $fPrice;
    private $oPrice;
    private $oPos;
    private $nPrice;
    
    public function __construct($productId, $fPrice, $oPrice, $oPos, $nPrice) {
        $this->productId = $productId;
        $this->fPrice = $fPrice;
        $this->oPrice = $oPrice;
        $this->oPos = $oPos;
        $this->nPrice = $nPrice;
    }
    
    public function getProductId() {
        return $this->productId;
    }

    public function getFPrice() {
        return $this->fPrice;
    }

    public function getOPrice() {
        return $this->oPrice;
    }

    public function getOPos() {
        return $this->oPos;
    }

    public function getNPrice() {
        return $this->nPrice;
    }

    public function setProductId($productId) {
        $this->productId = $productId;
        return $this;
    }

    public function setFPrice($fPrice) {
        $this->fPrice = $fPrice;
        return $this;
    }

    public function setOPrice($oPrice) {
        $this->oPrice = $oPrice;
        return $this;
    }

    public function setOPos($oPos) {
        $this->oPos = $oPos;
        return $this;
    }

    public function setNPrice($nPrice) {
        $this->nPrice = $nPrice;
        return $this;
    }
  
    public function getAction() {
        return 'update';
    }

}

